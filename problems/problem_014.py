# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

def can_make_pasta(ingredients):
    if ("eggs" in ingredients 
    and "flour" in ingredients
    and "oil" in ingredients):
        return True
    else:
        return False
