# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    res = ""
    #this will return a string
    if len(s) == 0:
        return s
    for x in s:
        if x not in res:
        #"if the letter is not in the string"
            res += x
            #equal to res = res + x
    return res      
    
