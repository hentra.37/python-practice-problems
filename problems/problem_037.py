# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    result = ""
    if length < len(str(number)):
        return str(number)
    else:
        difference = length - len(str(number))
        for x in range(difference):
        #Start at 0 and go until you hit variable difference
           result += pad
           #You're adding pad to the result string (which is empty)
    return result + str(number)
    # +str(number just adds the number to the end of it)
