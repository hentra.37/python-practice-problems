# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

def is_palindrome(word):
    if "".join(reversed(word)) == word:
#You use the join method because reversed(word) alone returns
# an array and join combines them into a string
        return True
    else:
        return False